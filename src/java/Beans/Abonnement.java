package Beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Un abonnement représente un logement raccordé au service Un même client peut avoir plusieurs abonnements.
 *
 * @author William Garos
 */
@Entity
public class Abonnement {

    /**
     * Référence unique et définitive de l'abonnement.
     */
    @Id
    @Column(unique = true, length = 11)
    private String ref;

    /**
     * Date de création de l'abonnement.
     */
     @Column(nullable = false)
    @Temporal(TemporalType.TIME)
    private Date dateAbonnement;

    /**
     * Adresse du logement concerné par l'abonnement.
     */
     @Column(nullable = false)
    private String adresse;

    /**
     * Code postal du logement concerné par l'abonnement.
     */
     @Column(nullable = false)
    private String cp;

    /**
     * Ville dans laquelle se situe le logement concerné par l'abonnement.
     */
     @Column(nullable = false)
    private String ville;

    /**
     * Liste des Compteurs Usager.
     */
     @OneToMany(mappedBy = "lAbonnement")
    private List<Usager> lesCompteursUsager = new ArrayList<>();

    /**
     * Le Client liée a l'abonnement.
     */
    @ManyToOne
    @JoinColumn(name = "numClient",nullable=false)
    private Client leClient;

    //Constructeurs
    /**
     * Constructeur vide.
     *
     */
    public Abonnement() {
    }

    //Accesseurs
    /**
     * Retourne la référence de l'abonnement.
     *
     * @return La référence de l'abonnement.
     */
    public String getRef() {
        return ref;
    }

    /**
     * Retourne la date de création du contrat.
     *
     * @return La date de création du contrat.
     */
    public Date getDateAbonnement() {
        return dateAbonnement;
    }

    /**
     * Retourne l'adresse du logement concerné par le contrat.
     *
     * @return L'adresse du logement concerné par le contrat.
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * Retourne le code postal du logement concerné par le contrat.
     *
     * @return Le code postal du logement concerné par le contrat.
     */
    public String getCp() {
        return cp;
    }

    /**
     * Retourne la ville du logement concerné par le contrat.
     *
     * @return La ville du logement concerné par le contrat.
     */
    public String getVille() {
        return ville;
    }

    /**
     * Retourne Le Client concerné par le contrat.
     *
     * @return Le Client concerné par le contrat.
     */
    public Client getLeClient() {
        return leClient;
    }

    /**
     * retourne la liste des Compteurs Usager.
     *
     * @return Liste des Compteurs Usager.
     */
    public List<Usager> getLesCompteursUsager() {
        return lesCompteursUsager;
    }

    //Mutateurs
    /**
     * Met à jour la référence de l'abonnement. Méthode obligatoire pour le fonctionnement d'Hibernate.
     *
     * @param ref La nouvelle référence de l'abonnement.
     */
    public void setRef(String ref) {
        this.ref = ref;
    }

    /**
     * Met à jour la date de création du contrat.
     *
     * @param dateAbonnement La nouvelle date de création de contrat.
     */
    public void setDateAbonnement(Date dateAbonnement) {
        this.dateAbonnement = dateAbonnement;
    }

    /**
     * Met à jour l'adresse du logement concerné par le contrat.
     *
     * @param adresse La nouvelle adresse du logement concerné par le contrat.
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * Met à jour le code postal du logement concerné par le contrat.
     *
     * @param cp Le nouveau code postal du logement concerné par le contrat.
     */
    public void setCp(String cp) {
        this.cp = cp;
    }

    /**
     * Met à jour la ville du logement concerné par le contrat.
     *
     * @param ville La nouvelle ville du logement concerné par le contrat.
     */
    public void setVille(String ville) {
        this.ville = ville;
    }

    /**
     * Met à jour le client concerné par le contrat.
     *
     * @param leClient Le Client concerné par le contrat.
     */
    public void setLeClient(Client leClient) {
        this.leClient = leClient;
    }

    /**
     * Met à jour la liste des Compteurs Usager.
     *
     * @param lesCompteursUsager La liste des Compteurs Usager mis a jour.
     */
    public void setLesCompteursUsager(List<Usager> lesCompteursUsager) {
        this.lesCompteursUsager = lesCompteursUsager;
    }

    //Méthodes
    /**
     * Ajoute un Compteur Usager à la liste des compteurs Usager.
     *
     * @param u Le nouveau Compteur Usager a ajouté.
     */
    public void addCompteurUsager(Usager u) {
        this.lesCompteursUsager.add(u);
    }
}
