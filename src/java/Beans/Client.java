package Beans;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Un client représente la personne qui a souscrit un contrat.
 *
 * @author William Garos
 */
@Entity
public class Client {

    /**
     * Identifiant unique et définitif du client.
     */
    @Id
    @Column(unique = true, length = 11)
    private int numClient;

    /**
     * Le nom de famille du client.
     */
    @Column(nullable = false)
    private String nomClient;

    /**
     * Le prénom du client.
     */
    @Column(nullable = false)
    private String prenomClient;

    /**
     * L'adresse personnelle du client.
     */
    @Column(nullable = false)
    private String adresseClient;

    /**
     * Le code postal de l'adresse personnelle du client.
     */
    @Column(nullable = false)
    private String cpClient;

    /**
     * La ville du logement personnel du client.
     */
    @Column(nullable = false)
    private String villeClient;

    /**
     * Liste des Abonnements.
     */
    @OneToMany(mappedBy = "leClient")
    private List<Abonnement> lesAbonnements = new ArrayList<>();

    //Constructeurs
    /**
     * Constructeur vide.
     *
     */
    public Client() {
    }

    //Accesseurs
    /**
     * Retourne l'identifiant du client.
     *
     * @return L'identifiant du client.
     */
    public int getNumClient() {
        return numClient;
    }

    /**
     * Retourne le nom de famille du client.
     *
     * @return Le nom de famille du client.
     */
    public String getNomClient() {
        return nomClient;
    }

    /**
     * Retourne le prénom du client.
     *
     * @return Le prénom du client.
     */
    public String getPrenomClient() {
        return prenomClient;
    }

    /**
     * Retourne l'adresse personnelle du client.
     *
     * @return L'adresse personnelle du client.
     */
    public String getAdresseClient() {
        return adresseClient;
    }

    /**
     * Retourne le code postal de l'adresse personnelle du client.
     *
     * @return Le code postal de l'adresse personnelle du client.
     */
    public String getCpClient() {
        return cpClient;
    }

    /**
     * Retourne la ville de l'adresse personnelle du client.
     *
     * @return La ville de l'adresse personnelle du client.
     */
    public String getVilleClient() {
        return villeClient;
    }

    /**
     * retourne la liste des Abonnement.
     *
     * @return Liste des Abonnements.
     */
    public List<Abonnement> getLesAbonnements() {
        return lesAbonnements;
    }

    //Mutateurs
    /**
     * Met à jour l'identifiant du client. Méthode obligatoire pour le fonctionnement d'Hibernate.
     *
     * @param numClient Le nouvel identifiant du client.
     */
    public void setNumClient(int numClient) {
        this.numClient = numClient;
    }

    /**
     * Met à jour le nom de famille du client.
     *
     * @param nomClient Le nouveau nom de famille du client.
     */
    public void setNomClient(String nomClient) {
        this.nomClient = nomClient;
    }

    /**
     * Met à jour le prénom du client.
     *
     * @param prenomClient Le nouveau prénom du client
     */
    public void setPrenomClient(String prenomClient) {
        this.prenomClient = prenomClient;
    }

    /**
     * Met à jour l'adresse personnelle du client.
     *
     * @param adresseClient La nouvelle adresse du client.
     */
    public void setAdresseClient(String adresseClient) {
        this.adresseClient = adresseClient;
    }

    /**
     * Met à jour le code postal de l'adresse personnelle du client.
     *
     * @param cpClient Le nouveau code postal de l'adresse personnelle du client.
     */
    public void setCpClient(String cpClient) {
        this.cpClient = cpClient;
    }

    /**
     * Met à jour la ville de l'adresse personnelle du client.
     *
     * @param villeClient La nouvelle ville de l'adresse personnelle du client.
     */
    public void setVilleClient(String villeClient) {
        this.villeClient = villeClient;
    }

    /**
     * Met à jour la liste des Abonnements.
     *
     * @param lesAbonnements La liste des Abonnements mis a jour.
     */
    public void setLesAbonnements(List<Abonnement> lesAbonnements) {
        this.lesAbonnements = lesAbonnements;
    }

    //Méthodes
    /**
     * Ajoute un Abonnement à la liste.
     *
     * @param a Le nouvelle abonnement a ajouté.
     */
    public void addAbonnement(Abonnement a) {
        this.lesAbonnements.add(a);
    }
}
