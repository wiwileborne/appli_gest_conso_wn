package Beans;

import java.util.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Une commune représente un lieu d'habitation, une ville.
 *
 * @author William Garos
 */
@Entity
public class Commune {

    /**
     * Numéro unique et définitif de la commune.
     */
    @Id
    @Column(unique = true, length = 11)
    private int numCom;

    /**
     * Nom de la commune.
     */
    @Column(nullable = false)
    private String nomCom;

    /**
     * Liste des secteurs dans la Commune.
     */
    @OneToMany(mappedBy="laCommune")
    private List<Secteur> lesSecteurs = new ArrayList<>();

    //Constructeurs
    /**
     * Constructeur vide.
     */
    public Commune() {
    }

    /**
     * Constructeur
     *
     * @param numCom numero de commune
     * @param nomCom nom de la commune
     */
    public Commune(int numCom, String nomCom) {
        this.numCom = numCom;
        this.nomCom = nomCom;
    }

    //Accesseurs
    /**
     * retourne la liste des secteurs.
     *
     * @return Liste de secteurs.
     */
    public List<Secteur> getLesSecteurs() {
        return lesSecteurs;
    }

    /**
     * retourne le numéro de la commune.
     *
     * @return Le numéro de la commune.
     */
    public int getNumCom() {
        return numCom;
    }

    /**
     * Retourne le nom de la commune.
     *
     * @return Le nom de la commune.
     */
    public String getNomCom() {
        return nomCom;
    }

     /**
     * Retourne le nom de la commune.
     *
     * @return Le nom de la commune sans espace.
     */
    public String getNomComTrim() {
        return nomCom.replaceAll(" ", "");
    }
    //Mutateurs
    /**
     * Met à jour la liste des secteurs.
     *
     * @param lesSecteurs La liste des secteurs mis a jour.
     */
    public void setLesSecteurs(List<Secteur> lesSecteurs) {
        this.lesSecteurs = lesSecteurs;
    }

    /**
     * Met à jour le numéro de la commune. Méthode obligatoire pour le fonctionnement d'Hibernate.
     *
     * @param numCom Le nouveau numéro de la commune.
     */
    public void setNumCom(int numCom) {
        this.numCom = numCom;
    }

    /**
     * Met à jour le nom de la commune.
     *
     * @param nomCom Le nouveau nom de la commune.
     */
    public void setNomCom(String nomCom) {
        this.nomCom = nomCom;
    }

    //Méthodes
    /**
     * Ajoute un secteur à la liste des secteurs de la commune.
     *
     * @param s Le nouveau secteur a ajouté.
     */
    public void addSecteur(Secteur s) {
        this.lesSecteurs.add(s);
    }

    //Méthodes
    /**
     * perte en eau de la commune.
     *
     * @return le total de la perte en eau de la commune
     */
    public int perteEau() {
        return volumeTotal("Vanne") - volumeTotal("Usager");
    }

    /**
     * Volume total consommer.
     *
     * @param typeCompteur le nom du compteur "Vanne" ou "Usager"
     * @return Volume Total
     */
    public int volumeTotal(String typeCompteur) {
        int tot = 0;
        for (Secteur s : lesSecteurs) {
            for (Compteur c : s.getLesCompteurs()) {
                if (c.getClass().getSimpleName().equals(typeCompteur)) {
                    tot += c.derniereConso();
                }
            }
        }
        return tot;
    }

    /**
     * Degré d'anomalie de la commune.
     *
     * @return 1,2,3 degré d'anomalie
     */
    public int degreAnomalies() {
        int anomalie = 0;
        float volume1 =  volumeTotal("Vanne") * 0.15F;
        float volume2 =  volumeTotal("Vanne") * 0.10F;
        if(perteEau() >= volume1){
            anomalie = 3;
        }else if (perteEau() < volume2){
            anomalie = 1;
        }else{
            anomalie = 2;
        }
        
        return anomalie;
    }

    /**
     * Renvoie la liste des espaces vert
     *
     * @return liste des espaces vert
     */
    public List<Secteur> espacesVert() {
        List<Secteur> listeR = new ArrayList<>();
        lesSecteurs.stream().filter((s) -> (s.getEspaceVert())).forEachOrdered(listeR::add);
        return listeR;
    }
}
