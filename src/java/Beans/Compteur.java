package Beans;

import java.util.*;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import static javax.persistence.DiscriminatorType.STRING;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import static javax.persistence.InheritanceType.SINGLE_TABLE;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Un compteur représente un appareil de relevés de consommation d'eau.
 *
 * @author William Garos
 */
@Entity
@Table(name="Compteur")
@Inheritance(strategy=SINGLE_TABLE)
@DiscriminatorColumn(name="typeCompteur",discriminatorType=STRING)
public abstract class Compteur {

    /**
     * Référence unique et définitive du compteur.
     */
    @Id
    @Column(unique = true, length = 11)
    private String refCompteur;

    /**
     * Date d'installation du compteur.
     */
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dateInstallation;

    /**
     * Marque du compteur.
     */
    @Column(nullable = false)
    private String marque;

    /**
     * Liste des Releve du compteur.
     */
    @OneToMany(mappedBy="leCompteur")
    private List<Releve> lesReleves = new ArrayList<>();

    //Constructeur
    /**
     * Constructeur vide.
     */
    public Compteur() {
    }

    /**
     * Constructeur
     * @param refCompteur reference du compteur
     * @param dateInstallation date d'installation du compteur
     * @param marque marque du compteur
     */
    public Compteur(String refCompteur, Date dateInstallation, String marque) {
        this.refCompteur = refCompteur;
        this.dateInstallation = dateInstallation;
        this.marque = marque;
    }

    //Accesseurs
    /**
     * Retourne la référence du compteur.
     *
     * @return La référence du compteur.
     */
    public String getRefCompteur() {
        return refCompteur;
    }

    /**
     * Retourne la date d'installation du compteur.
     *
     * @return La date d'installation du compteur.
     */
    public Date getDateInstallation() {
        return dateInstallation;
    }

    /**
     * Retourne la marque du compteur.
     *
     * @return La marque du compteur.
     */
    public String getMarque() {
        return marque;
    }

    /**
     * retourne la liste des Releves.
     *
     * @return Liste de Releves.
     */
    public List<Releve> getLesReleves() {
        return lesReleves;
    }
    //Mutateurs

    /**
     * Met à jour la référence du compteur. Méthode obligatoire pour le fonctionnement d'Hibernate.
     *
     * @param refCompteur La nouvelle référence du compteur.
     */
    public void setRefComteur(String refCompteur) {
        this.refCompteur = refCompteur;
    }

    /**
     * Met à jour la date d'installation du compteur.
     *
     * @param dateInstallation La nouvelle date d'installation du compteur.
     */
    public void setDateInstallation(Date dateInstallation) {
        this.dateInstallation = dateInstallation;
    }

    /**
     * Met à jour la marque du compteur.
     *
     * @param marque La nouvelle marque du compteur.
     */
    public void setMarque(String marque) {
        this.marque = marque;
    }

    /**
     * Met à jour la liste des Releves.
     *
     * @param lesReleves La liste des Releves mis a jour.
     */
    public void setLesReleves(List<Releve> lesReleves) {
        this.lesReleves = lesReleves;
    }

    //Méthodes
    /**
     * Ajoute un Releve à la liste des Releves du compteur.
     *
     * @param r Le nouveau Releve a ajouté.
     */
    public void addReleve(Releve r) {
        this.lesReleves.add(r);
    }

    /**
     * Retourne l'addition de la conssomation des 2 dernier relevé.
     *
     * @return le total des 2 derniere conso
     */
    public int derniereConso() {
        
        int total = 0;
        
        
        if(lesReleves.size()==1){
        total = lesReleves.get(lesReleves.size() - 1).getIndexReleve();
        }else if(lesReleves.size()>1){
            lesReleves.sort(Comparator.comparing(o -> o.getDateReleve()));
        total = lesReleves.get(lesReleves.size() - 1).getIndexReleve() - lesReleves.get(lesReleves.size() - 2).getIndexReleve();
        }
        return total ;
    }

}
