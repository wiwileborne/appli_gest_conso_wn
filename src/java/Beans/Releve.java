package Beans;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Un relevé représente un relevé de consommation d'eau d'un logement.
 * @author William Garos
 */
@Entity
public class Releve {
    
    /**
     * Numéro unique et définitif du relevé.
     */
    @Id
    @Column(unique = true, length = 11)
    @GeneratedValue(strategy = IDENTITY)
    private int numReleve;
    
    /**
     * Date de création du relevé.
     */
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dateReleve;
   
    /**
     * Volume d'eau relevé.
     */
    @Column(nullable = false)
    private int indexReleve;  
        
     /**
     * Le Compteur depuis lequel est prit le Releve.
     */
    @ManyToOne
    @JoinColumn(name = "numCompteur",nullable=false)
    private Compteur leCompteur;
    //Constructeurs
    
    /**
     * Constructeur vide.
     */
    public Releve() {
    }

    public Releve(int numReleve, Date dateReleve, int indexReleve, Compteur leCompteur) {
        this.numReleve = numReleve;
        this.dateReleve = dateReleve;
        this.indexReleve = indexReleve;
        this.leCompteur = leCompteur;
    }
    
    
    //Accesseurs
    
    /**
     * Retourne le numéro du relevé.
     * 
     * @return Le numéro du relevé.
     */
    public int getNumReleve() {
        return numReleve;
    }

    /**
     * Retourne la date du relevé.
     * 
     * @return La date du relevé.
     */
    public Date getDateReleve() {
        return dateReleve;
    }

    /**
     * Retourne l'index du relevé, c'est à dire le volume d'eau consommé à cette date.
     * 
     * @return L'index du relevé.
     */
    public int getIndexReleve() {
        return indexReleve;
    }

    /**
     * Retourne le compteur du releve.
     * 
     * @return le compteur du relevé.
     */
    public Compteur getLeCompteur() {
        return leCompteur;
    }
    
    
    
    //Mutateurs
    
    /**
     * Met à jour le numéro de relevé. Méthode obligatoire pour le fonctionnement d'Hibernate.
     * 
     * @param numReleve Le nouveau numéro de relevé.
     */
    public void setNumReleve(int numReleve) {
        this.numReleve = numReleve;
    }

    /**
     * Met à jour la date de création du relevé.
     * 
     * @param dateReleve La nouvelle date de relevé.
     */
    public void setDateReleve(Date dateReleve) {
        this.dateReleve = dateReleve;
    }

    /**
     * Met à jour l'index du compteur du relevé.
     * @param indexReleve le volume d'eau consommé
     */
    public void setIndexReleve(int indexReleve) {
        this.indexReleve = indexReleve;
    }    

    /**
     * Met à jour le compteur du relevé.
     * @param leCompteur le Compteur du relevé
     */
    public void setLeCompteur(Compteur leCompteur) {
        this.leCompteur = leCompteur;
    }
    
    
}
