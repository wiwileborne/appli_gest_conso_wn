package Beans;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Un secteur représente une zone géographique, ou quartier, d'une commune. Une commune a plusieurs secteurs.
 *
 * @author William Garos
 */
@Entity
public class Secteur {
    //Déclaration des attributs de la table

    /**
     * Numéro unique et définitif de secteur.
     */
    @Id
    @Column(unique = true, length = 11)
    @GeneratedValue(strategy = IDENTITY)
    private int numSecteur;

    /**
     * Nom du quartier/secteur.
     */
    @Column( nullable = false)
    private String nomSecteur;

    /**
     * La présence ou non d'au moins un espace vert dans le secteur. La valeur 1 vaut Oui.
     */
    @Column( nullable = false)
    private Boolean espaceVert;

    /**
     * La Commune dans laquelle se trouve le secteur.
     */
    @ManyToOne
    @JoinColumn(name = "numCommune",nullable=false)
    private Commune laCommune;
    //Constructeurs

    /**
     * Liste des Compteurs du secteur.
     */
    @OneToMany
    @JoinColumn(name = "numSecteur",nullable=false)
    private List<Compteur> lesCompteurs = new ArrayList<>();

    /**
     * Constructeur vide.
     */
    public Secteur() {
    }

    //Accesseurs
    /**
     * Retourne le numéro du secteur.
     *
     * @return Le numéro du secteur.
     */
    public int getNumSecteur() {
        return numSecteur;
    }

    /**
     * Retourne le nom du secteur.
     *
     * @return Le nom du secteur.
     */
    public String getNomSecteur() {
        return nomSecteur;
    }

    /**
     * Retourne la présence ou non d'un espace vert dans le secteur.
     *
     * @return La présence ou non d'un espace vert dans le secteur.
     */
    public Boolean getEspaceVert() {
        return espaceVert;
    }

    /**
     * Retourne la Commune du secteur.
     *
     * @return La commune du secteur.
     */
    public Commune getLaCommune() {
        return laCommune;
    }

    /**
     * retourne la liste des Compteurs.
     *
     * @return Liste de Compteurs.
     */
    public List<Compteur> getLesCompteurs() {
        return lesCompteurs;
    }

    //Mutateurs
    /**
     * Met à jour le numéro du secteur.
     *
     * @param numSecteur Le nouveau numéro de secteur.
     */
    public void setNumSecteur(int numSecteur) {
        this.numSecteur = numSecteur;
    }

    /**
     * Met à jour le nom du secteur.
     *
     * @param nomSecteur Le nouveau nom du secteur.
     */
    public void setNomSecteur(String nomSecteur) {
        this.nomSecteur = nomSecteur;
    }

    /**
     * Met à jour la présence ou non d'un espace vert dans le secteur.
     *
     * @param espaceVert La présence ou non d'un espace vert dans le secteur.
     */
    public void setEspaceVert(Boolean espaceVert) {
        this.espaceVert = espaceVert;
    }

    /**
     * Met à jour la Commune du secteur.
     *
     * @param c La nouvelle Commune du secteur.
     */
    public void setLaCommune(Commune c) {
        this.laCommune = c;
    }

    /**
     * Met à jour la liste des Compteurs.
     *
     * @param lesCompteurs La liste des Compteurs mis a jour.
     */
    public void setLesCompteurs(List<Compteur> lesCompteurs) {
        this.lesCompteurs = lesCompteurs;
    }

    //Méthodes
    /**
     * Ajoute un Compteur à la liste des compteurs du secteur.
     *
     * @param c Le nouveau Compteur a ajouté.
     */
    public void addCompteur(Compteur c) {
        this.lesCompteurs.add(c);
    }
}
