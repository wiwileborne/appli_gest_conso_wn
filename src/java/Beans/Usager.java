package Beans;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Un secteur représente une zone géographique, ou quartier, d'une commune. Une commune a plusieurs secteurs.
 *
 * @author William Garos
 */
@Entity
@DiscriminatorValue(value = "Usager")
public class Usager extends Compteur {
    //Déclaration des attributs de la table

    /**
     * L'Abonnement du compteur usager.
     */
    @ManyToOne
    @JoinColumn(name = "numAbonnement")
    private Abonnement lAbonnement;

    /**
     * Constructeur vide.
     */
    public Usager() {
    }

    //Accesseurs
    /**
     * Retourne L'abonnement.
     *
     * @return l'abonnement du compteur usager.
     */
    public Abonnement getlAbonnement() {
        return lAbonnement;
    }

    //Mutateurs
    /**
     * Met à jour la Commune du secteur.
     *
     * @param a Le nouveau abonnement du compteur usager.
     */
    public void setlAbonnement(Abonnement a) {
        this.lAbonnement = a;
    }

}
