package Beans;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Un secteur représente une zone géographique, ou quartier, d'une commune. Une commune a plusieurs secteurs.
 *
 * @author WilliamGaros
 */
@Entity
@DiscriminatorValue(value = "Vanne")
public class Vanne extends Compteur {
    //Déclaration des attributs de la table

    /**
     * Constructeur vide.
     */
    public Vanne() {
    }

    //Accesseurs
    //Mutateurs
}
