/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * Document   : ListeQuartierVanne
 * Created on : 9 déc. 2019, 14:42:01
 * Author     : william.garos
 */
package Servlets;

import Beans.Commune;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import passerelle.DAOCommune;

/**
 *
 * @author william.garos
 */
@WebServlet(name = "listeCommunes", urlPatterns = "/liste-communes")
public class ListeQuartierVanne extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        List<Commune> listeC = DAOCommune.getListeCommune();
        request.setAttribute("lesCommunes", listeC);
        this.getServletContext().getRequestDispatcher("/pages/v_listeCommune.jsp").forward(request, response);
    }

}