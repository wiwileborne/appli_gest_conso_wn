/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Beans.Compteur;
import Beans.Vanne;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import passerelle.DAOSecteur;

/**
 *
 * @author william.garos
 */
@WebServlet(name = "leSecteur", urlPatterns = "/leSecteur")
public class leSecteur extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Beans.Secteur s = DAOSecteur.getSecteurById(request.getParameter("id"));
        List<Compteur> listC = new ArrayList<>();
          for (Compteur c : s.getLesCompteurs()) {
                    if (c instanceof Vanne) {
                        listC.add(c);
                        System.out.println(c.getMarque());
                    }
          }
        request.setAttribute("Secteur", s);
        request.setAttribute("Compteurs", listC);
        this.getServletContext().getRequestDispatcher("/pages/v_secteur.jsp").forward(request, response);
    }

}