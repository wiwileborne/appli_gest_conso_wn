
package Servlets;

import Beans.Commune;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import passerelle.DAOCommune;

/**
 *
 * @author william.garos
 */
@WebServlet(name = "listeDegreAnomalie", urlPatterns = "/listeDegreAnomalie")
public class listeDegreAnomalie extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        List<Commune> listeC = DAOCommune.getListeCommune();
        request.setAttribute("lesCommunes", listeC);
        this.getServletContext().getRequestDispatcher("/pages/v_listeAnomalie.jsp").forward(request, response);
    }

}