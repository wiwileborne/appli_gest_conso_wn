
package Servlets;

import Beans.Commune;
import Beans.Secteur;
import java.io.IOException;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import passerelle.DAOCommune;

/**
 *
 * @author william.garos
 */
@WebServlet(name = "listeSecteur", urlPatterns = "/listeSecteur")
public class listeSecteur extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Commune laCommune = DAOCommune.getCommuneID(request.getParameter("idCommune"));
        List<Secteur> listSecteur = laCommune.getLesSecteurs();
        String json = "{\"" + laCommune.getNomCom() + "\": [";
        for(Secteur s : laCommune.getLesSecteurs()){
            if(laCommune.getLesSecteurs().indexOf(s) != laCommune.getLesSecteurs().size()-1){
                json+= "{"
                        + "\"numSecteur\" : \"" + s.getNumSecteur() + "\"," 
                        + "\"nomSecteur\" : \"" + s.getNomSecteur() + "\"," 
                        + "\"EspaceVert\" : \"" + s.getEspaceVert() + "\"" +
                       "},";
            }else {
                json+= "{"
                        + "\"numSecteur\" : \"" + s.getNumSecteur() + "\"," 
                        + "\"nomSecteur\" : \"" + s.getNomSecteur() + "\"," 
                        + "\"EspaceVert\" : \"" + s.getEspaceVert() + "\"" +
                       "}]}";
            }
        }     
        response.setContentType("application/json");
        response.getWriter().write(json);
    }

}