/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filtres;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

/**
 *
 * @author william.garos
 */
@WebFilter(filterName = "FiltreUTF8", urlPatterns = {"/*"})
public class FiltreUTF8 implements Filter { 
    public FiltreUTF8() {
    }    
    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws UnsupportedEncodingException, IOException, ServletException{
      
      request.setCharacterEncoding("UTF-8");
      response.setContentType("text/html;charset=UTF8");
      chain.doFilter(request, response);
    }
    /**
     * Destroy method for this filter
     */
    public void destroy() {        
    }
    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {        
       
    }
}
