/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passerelle;

import Beans.Commune;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author william.garos
 */
public class DAOClient {

    public static List<Commune> getListeConcours() {
        Session session = HibernateUtil.getSession();
        return session.createQuery("from Commune")
                .list();
    }
}
