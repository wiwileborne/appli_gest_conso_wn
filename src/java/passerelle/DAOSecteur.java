/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passerelle;

import Beans.Secteur;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author william.garos
 */
public class DAOSecteur {

    public static List<Secteur> getListeSecteurs() {
        Session session = HibernateUtil.getSession();
        return session.createQuery("from Secteur")
                .list();
    }
    
     public static Secteur getSecteurById(String id) {
        Session session = HibernateUtil.getSession();
        Query query = session.createQuery("from Secteur where id =" + id);
        Secteur s = (Secteur) query.uniqueResult();
        return s;
    }
}
