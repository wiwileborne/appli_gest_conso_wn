/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * @author William Garos
 */
package Beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author william.garos
 */
public class CommuneTest {

    public CommuneTest() {
    }
    Commune laCommune = new Commune(0, "La Commune");
    Commune laCommune1 = new Commune(0, "La Commune");
    Releve leReleve3;
    Releve leReleve10;
    Secteur s1;
    Secteur s2;
    Secteur s3;
    Secteur s4;
    Secteur s5;

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        s1 = new Secteur();
        s1.setEspaceVert(true);
        s2 = new Secteur();
        s2.setEspaceVert(false);
        s3 = new Secteur();
        s3.setEspaceVert(true);
        s4 = new Secteur();
        s4.setEspaceVert(false);
        s5 = new Secteur();
        s4.setEspaceVert(false);

        laCommune.addSecteur(s1);
        laCommune.addSecteur(s2);
        laCommune.addSecteur(s3);
        laCommune.addSecteur(s4);
        laCommune1.addSecteur(s1);
        laCommune1.addSecteur(s2);
        laCommune1.addSecteur(s5);

        Compteur leCompteur = new Vanne();
        Compteur leCompteur1 = new Vanne();
        Compteur leCompteur2 = new Usager();
        Compteur leCompteur3 = new Usager();
        Compteur leCompteur4 = new Vanne();
        Compteur leCompteur5 = new Vanne();
        Compteur leCompteur6 = new Usager();
        Compteur leCompteur7 = new Usager();

        Releve leReleve1 = new Releve(0, new Date(2019, 10, 1), 20, leCompteur);
        Releve leReleve2 = new Releve(1, new Date(2019, 11, 1), 50, leCompteur1);
        leReleve3 = new Releve(2, new Date(2019, 12, 1), 10, leCompteur2);
        Releve leReleve4 = new Releve(0, new Date(2019, 10, 1), 20, leCompteur3);
        Releve leReleve5 = new Releve(1, new Date(2019, 11, 1), 50, leCompteur4);
        Releve leReleve6 = new Releve(2, new Date(2019, 12, 1), 150, leCompteur5);
        Releve leReleve7 = new Releve(0, new Date(2019, 10, 1), 20, leCompteur6);
        Releve leReleve8 = new Releve(1, new Date(2019, 11, 1), 50, leCompteur7);
        Releve leReleve9 = new Releve(2, new Date(2019, 12, 1), 150, leCompteur1);
        leReleve10 = new Releve(2, new Date(2019, 12, 1), 250, leCompteur1);

        leCompteur.addReleve(leReleve1);//20
        leCompteur1.addReleve(leReleve2);//50
        leCompteur2.addReleve(leReleve3);
        leCompteur3.addReleve(leReleve4);
        leCompteur4.addReleve(leReleve5);//50
        leCompteur5.addReleve(leReleve6);//150
        leCompteur6.addReleve(leReleve7);
        leCompteur7.addReleve(leReleve8);
        leCompteur1.addReleve(leReleve9);//150
        leCompteur6.addReleve(leReleve10);//150
        s1.addCompteur(leCompteur);
        s2.addCompteur(leCompteur1);
        s3.addCompteur(leCompteur2);
        s4.addCompteur(leCompteur3);
        s1.addCompteur(leCompteur4);
        s2.addCompteur(leCompteur5);
        s3.addCompteur(leCompteur6);
        s4.addCompteur(leCompteur7);
        s5.addCompteur(leCompteur6);

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of perteEau method, of class Commune.
     */
    @Test
    public void testPerteEau() {
        assertEquals(10, laCommune.perteEau(), 0);

    }

    /**
     * Test of volumeTotal method, of class Commune.
     */
    @Test
    public void testVolumeTotal() {
        assertEquals(320, laCommune.volumeTotal("Vanne"), 0);
        
        assertEquals(310, laCommune.volumeTotal("Usager"), 0);

    }

    /**
     * Test of degreAnomalies method, of class Commune.
     */
    @Test
    public void testDegreAnomalies() {
        assertEquals(1, laCommune.degreAnomalies(), 0);
        leReleve10.setIndexReleve(300);
        assertEquals(2, laCommune1.degreAnomalies(), 0);
        leReleve10.setIndexReleve(250);
        assertEquals(3, laCommune1.degreAnomalies(), 0);
    }

    /**
     * Test of espacesVert method, of class Commune.
     */
    @Test
    public void testEspacesVert() {
        
        //Test Si 2 espace Vert
        List<Secteur> deuxTrue = new ArrayList<>();
        deuxTrue.add(s1);
        deuxTrue.add(s3);
        assertEquals(deuxTrue, laCommune.espacesVert());
        
        //Test Si liste vide
        deuxTrue.clear();
        s3.setEspaceVert(false);
        s1.setEspaceVert(false);
        assertEquals(deuxTrue,laCommune.espacesVert());
    }
}
