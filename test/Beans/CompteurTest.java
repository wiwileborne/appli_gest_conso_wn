/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * @author William Garos
 */
package Beans;

import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author william.garos
 */
public class CompteurTest {
    
    public CompteurTest() {
    }
     Usager leCompteur = new Usager(); 
      Usager leCompteur1 = new Usager(); 
       Usager leCompteur2 = new Usager(); 
        
    @BeforeClass
    public static void setUpClass() {
        
       
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Releve leReleve1 = new Releve(0,new Date(2019,10,1),20,leCompteur); 
        Releve leReleve2 = new Releve(1,new Date(2019,11,1),50,leCompteur); 
        Releve leReleve3 = new Releve(2,new Date(2019,12,1),150,leCompteur); 
        leCompteur.addReleve(leReleve1);
        leCompteur.addReleve(leReleve2);
        leCompteur.addReleve(leReleve3);
        
        leCompteur1.addReleve(leReleve3);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of derniereConso method, of class Compteur.
     */
    @Test
    public void testDerniereConso() {
        
        assertEquals(100,leCompteur.derniereConso(),0);
        assertEquals(150,leCompteur1.derniereConso(),0);
        assertEquals(0,leCompteur2.derniereConso(),0);
    }
    
}
