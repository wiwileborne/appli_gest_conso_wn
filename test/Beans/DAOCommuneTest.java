/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * @author William Garos
 */
package Beans;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import passerelle.DAOCommune;

/**
 *
 * @author william.garos
 */
public class DAOCommuneTest {

    public DAOCommuneTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of testListeCommune method
     */
    @Test
    public void testListeCommune() {

        for (Commune com : DAOCommune.getListeCommune()) {
            System.out.println("------------------ Commune n°" + com.getNumCom() + " ------------------");
            System.out.println("Nom : " + com.getNomCom());
            for (Secteur ev : com.espacesVert()) {
                System.out.println("Espace vert du secteur : " + ev.getNomSecteur());

            }
            for (Secteur s : com.getLesSecteurs()) {
                System.out.println("-------------------- Secteur n°" + s.getNumSecteur() + " --------------------");
                System.out.println("Nom : " + s.getNomSecteur());

                for (Compteur c : s.getLesCompteurs()) {
                    System.out.println("---------------- Compteur ref:" + c.getRefCompteur() + " ----------------");
                    System.out.println("Marque : " + c.getMarque());
                    System.out.println("Date Installation : " + c.getDateInstallation());
                    if (c instanceof Vanne) {
                        System.out.println("Type : Vanne");
                    } else if (c instanceof Usager) {
                        System.out.println("Type : Usager");
                        Abonnement a = ((Usager) c).getlAbonnement();
                        System.out.println("---------------- Abonnement ref:" + a.getRef() + " ----------------");

                        System.out.println("Adresse : " + a.getAdresse());
                        System.out.println("CP : " + a.getCp());
                        System.out.println("Ville : " + a.getVille());
                        System.out.println("Date Abonnement : " + a.getDateAbonnement());
                        Client cl = a.getLeClient();
                        System.out.println("---------------- Client n°" + cl.getNumClient() + " ----------------");
                        System.out.println("Nom : " + cl.getNomClient());
                        System.out.println("Prenom : " + cl.getPrenomClient());
                        System.out.println("Adresse : " + cl.getAdresseClient());
                        System.out.println("Ville : " + cl.getVilleClient());
                        System.out.println("CP : " + cl.getCpClient());

                        System.out.println("-----------------------------------------------------");

                        System.out.println("-----------------------------------------------------");
                    }
                    for (Releve r : c.getLesReleves()) {
                        System.out.println("---------------- Releve n°" + r.getNumReleve() + " ----------------");
                        System.out.println("Index Releve : " + r.getIndexReleve());
                        System.out.println("Date Releve : " + r.getDateReleve());
                        System.out.println("-----------------------------------------------------");
                    }

                    System.out.println("-----------------------------------------------------");
                }

                System.out.println("-----------------------------------------------------");
            }

            System.out.println("------------------FIN COMMUNE------------------------");
            System.out.println("");
            System.out.println("");
        }
    }
}
