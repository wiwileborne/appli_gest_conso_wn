<%-- 
    Document   : v_listeAnomalie
    Created on : 9 déc. 2019, 14:42:01
    Author     : nicolas.Bertrand
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../templates/entete.jsp" %>
<%@include file="../templates/nav.jsp" %>
<div class="container body_container">
    <h1>Anomalies des communes</h1>
    <div class="form-row">
        <table class="table table-dark">
            <thead>
                <tr>
                    <th scope="col">Nom de la commune</th>
                    <th scope="col">Volume total distribuer par les vannes</th>
                    <th scope="col">perte en eau</th>
                    <th scope="col">Nombre de secteur Espace Vert</th>
                    <th scope="col">Degré d'anomalie</th>
                    <th scope="col">Quartier d'une commune</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${lesCommunes}" var="laCommune">
                <tr>
                    <td>${laCommune.getNomCom()}</td>
                    <td>${laCommune.volumeTotal("Vanne")}</td>
                    <td>${laCommune.perteEau()}</td>
                    <td>${laCommune.espacesVert().size()}</td>
                    <td>
                        <c:choose>
                            <c:when test="${laCommune.degreAnomalies() == 1}"><span class="badge badge-success">Degré 1</span></c:when>
                            <c:when test="${laCommune.degreAnomalies() == 2}"><span class="badge badge-warning">Degré 2</span></c:when>
                            <c:when test="${laCommune.degreAnomalies() == 3}"><span class="badge badge-danger">Degré 3</span></c:when>
                        </c:choose>
                    </td>
                    <td>
                        <button type="button" class="btn btn-primary secteur" data-toggle="modal" data-target="#exampleModal" id="${laCommune.getNumCom()}">
                            <i class="fas fa-search-plus"></i>
                        </button>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
  <thead>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body" id="tabSecteur">
            <table class="table table-dark">
            <thead>
                <tr>
                    <th scope="col">Numéro du quartier</th>
                    <th scope="col">Nom du quartier</th>
                    <th scope="col">Espace Vert</th>
                </tr>
            </thead>
            <tbody class="tabSecteur">
                
            </tbody>
            </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</body>
</html>
<script>
$(".secteur").click(function(){ 
    var idSecteur = this.id;
    $.ajax({
       url : 'listeSecteur',
       type : 'GET',
       data: 'idCommune=' + idSecteur,
       dataType : 'json',
       success : function(data){ 
           $(".tabSecteur").empty();
            $.each(data, function (key, value){
                $(".modal-title").text("Liste des quartiers de la commune : " + key);
                $.each(value, function (key, commune){
                    var tr = document.createElement('tr');
                    var numSecteurs = document.createElement('td');
                    var nomSecteurs = document.createElement('td');
                    var espaceVerts = document.createElement('td');

                    numSecteurs.innerHTML = commune.numSecteur;
                    nomSecteurs.innerHTML = commune.nomSecteur;
                    if(commune.EspaceVert == "true"){
                        espaceVerts.innerHTML = "Ce quartier contient un ou plusieurs espace vert";
                    }else{
                        espaceVerts.innerHTML = "Ce quartier ne contient pas un ou plusieurs espace vert";
                    }
                    tr.append(numSecteurs);
                    tr.append(nomSecteurs);
                    tr.append(espaceVerts);
                    $(".tabSecteur").append(tr);
                });
            }); 
       }
    }); 
});
</script>