<%-- 
    Document   : v_listeCommune
    Created on : 9 déc. 2019, 14:42:01
    Author     : william.garos
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="../templates/entete.jsp" %>
<%@include file="../templates/nav.jsp" %>
<div class="container body_container_mw">
    <h1>Liste des Communes</h1>
    <div class="form-row">
        <div class="form-group col-md-12">
            <div id="accordion">
                <c:forEach items="${lesCommunes}" var="uneCommune">
                    <div class="card">
                        <button class="btn btn-link " data-toggle="collapse" data-target="#${uneCommune.getNomComTrim()}" aria-expanded="true" aria-controls="${uneCommune.getNomComTrim()}">
                            <div class="card-header" id="heading${uneCommune.getNomComTrim()}">
                                <h4 class="mb-0">   ${uneCommune.getNomCom()}</h4>
                            </div>
                        </button>
                        <div id="${uneCommune.getNomComTrim()}" class="collapse" aria-labelledby="heading${uneCommune.getNomComTrim()}" data-parent="#accordion">
                            <div class="card-body">
                                <table style="border: 1px solid #333" class="table table-striped table-light">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">Nom du Secteur</th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        <c:forEach items="${uneCommune.lesSecteurs}" var="unSecteur">
                                            <tr>
                                                <td> ${unSecteur.getNomSecteur()} </td>
                                                <td class="text-center">
                                                    <a class="btn btn-primary" role="button" href="leSecteur?id=${unSecteur.getNumSecteur()}">
                                                        <i class="fas fa-search-plus"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
</body>
</html>










