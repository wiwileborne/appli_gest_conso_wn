<%-- 
    Document   : v_secteur
    Created on : 9 déc. 2019, 14:42:01
    Author     : william.garos
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="../templates/entete.jsp" %>
<%@include file="../templates/nav.jsp" %>
<div class="container body_container_mw ">
<h1>${Secteur.getNomSecteur()}</h1>

<div class="form-row">
    <div class="form-group col-md-12">
        <table  class="table table-striped table-light">
            <thead class="thead-dark">
            <tr>
                <th style="border: 1px solid #333">Ref Compteur</th>
                <th style="border: 1px solid #333">Marque</th>
                <th style="border: 1px solid #333">Date d'installation</th>
                <th style="border: 1px solid #333">Consommation</th>
            </tr>
            </thead>
            <c:forEach items="${Compteurs}" var="unCompteur">


                <tr>
                    <td style="border: 1px solid #333"> ${unCompteur.getRefCompteur()} </td>
                    <td style="border: 1px solid #333"> ${unCompteur.getMarque()}</td>
                    <td style="border: 1px solid #333"> ${unCompteur.getDateInstallation()}</td>
                    <td style="border: 1px solid #333"> ${unCompteur.derniereConso()}</td>

                </tr>

            </c:forEach>
        </table>
    </div>
</div>

</div>
</body>
</html>
